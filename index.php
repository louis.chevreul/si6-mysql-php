<?php

$connexion = mysqli_connect("172.16.99.3", "b.cordier", "passe", "b.cordier", 3306);
if(mysqli_connect_errno($connexion)){
	die("Connexion impossible : " . mysqli_connect_error());
}
else{
	$resultat = mysqli_query($connexion, "SELECT * FROM Adherent");

	if($resultat != false){
	    $modeHTML = False;

	if(isset($argc))
	{
		$modeHTML = True;
	}

	if($modeHTML)
	{
		print("

		<!DOCTYPE html>

		<html>

		<head>

		<title> TABLEAU EN HTML </title>

		<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" media=\"screen\"/>

		</head>

		<body>

		<table>

		");
	}

		$nombre_tuples = mysqli_num_rows($resultat);
		print("La requete a retourne " . $nombre_tuples . " tuple");
		print($nombre_tuples>1 ? 's':'');
		print("\n");

		$numero_tuple = 1;
		$cpt = 0;

		function afficher_ligne($tuple_a_afficher, $afficher_en_tete = FALSE, $modeHTML = False){

			$i= 0;

			foreach($tuple_a_afficher as $key => $value){
				print("<tr id='fond_menu'>".($afficher_en_tete ? $key : $value) . "</tr>"  . ($i == count($tuple_a_afficher) -1?"\n":($i % 2 == 0?"/":"|")));
				$i++;
			}
		}

		while($tuple = mysqli_fetch_assoc($resultat)){

			if($cpt % 5 == 0){

				afficher_ligne($tuple, TRUE);
			}

			else
			{

				afficher_ligne($tuple);

			}
			$numero_tuple++;
			$cpt--;

		}

		if(is_resource($resultat)){
			mysqli_free_result($resultat);
		}
	}
	mysqli_close($connexion);
}

if($modeHTML)
{
print("
	</table>

	</body>

	</html>
");
}

?>
